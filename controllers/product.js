const Product = require('../models/Product');


//Add Product
module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		category: reqBody.category,
		price: reqBody.price,
		img: reqBody.img,
		inStock: reqBody.inStock,
	})

	return newProduct.save().then(()=>{
		return newProduct
	}).catch(()=> {
		return false
	})
}

// Retrieve all Products
module.exports.getAllProducts = () => {
	return Product.find({}).sort({createdOn: -1}).then(result => {
		return result;
	});
};

// Retrieve available products
module.exports.getAllActive = () => {

	return Product.find({isActive: true}).sort({createdOn: -1}).then(result => {
		return result;
	});
};

// Retrieve a specific product
module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then(result => {

		let product = {
			name: result.name,
			description: result.description,
			category: result.category,
			price: result.price,
			inStock: result.inStock,
			img: result.img
		}
		return product
	})
}

//Search Product

module.exports.searchProduct = (reqBody) => {

	return Product.find({name: reqBody.name}).then(result => {
		return result
	}).catch((err)=> {
		return err
	})
}


// Retrieve by Category
module.exports.getCategory = (reqParams) => {
	return Product.find({category: reqParams.category, isActive: true}).sort({createdOn: -1}).then(result => {
		return result
	})
}

//Update Product 
module.exports.updateProduct = async(userData, reqParams, reqBody) => {
	if(userData.isAdmin === true){
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			category: reqBody.category,
			price: reqBody.price,
			//-----KVC START-----------//
			// inStock: reqBody.inStock,
			// img: reqBody.img
			//-----KVC END-----------//

		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then(()=> {
			return updatedProduct
		}).catch(()=>{
			return false
		})
	} else {
		return ('Not Authorized')
	}
}

//Archive Product
module.exports.archiveProduct = (reqParams) => {

	return Product.findByIdAndUpdate(reqParams.productId).then(result=> {
		if(result.isActive === true){

			result.isActive = false
			return result.save().then(()=> {
				return result
			}).catch(()=> {
				return false
			})
		} else {
			return result
		}
	})
}


//Reactivate Archived Product
module.exports.reactivateProduct = (reqParams) => {

	return Product.findByIdAndUpdate(reqParams.productId).then(result=> {
		if(result.isActive === false){

			result.isActive = true
			return result.save().then(()=> {
				return result
			}).catch(()=> {
				return false
			})
		} else {
			return result
		}
	})
}



